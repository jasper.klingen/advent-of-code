using AoCday1puzzle1;
using System.Collections.Generic;
using Xunit;

namespace AoCTest
{
    public class UnitTest1
    {
        public List<int> MockDepthList { get; set; } =  new List<int>{ 199, 200, 208, 210, 200, 207, 240, 269, 260, 263 };

        [Fact]
        public void CountIncreases_ListOfDepths_Seven()
        {
            //Arrange
            Depth depth = new Depth(MockDepthList);
            int expected = 7;
            //Act
            int actual = depth.CountIncreases(depth.DepthMeasurements);
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void CountIncreasesReduced_ListOfDepths_Five()
        {
            //Arrange
            Depth depth = new Depth(MockDepthList);
            depth.ReducedDepths = depth.ReduceDepthMeasurements(depth.DepthMeasurements);
            int expected = 5;
            //Act
            int actual = depth.CountIncreases(depth.ReducedDepths);
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
