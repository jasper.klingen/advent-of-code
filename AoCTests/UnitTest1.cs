using Microsoft.VisualStudio.TestTools.UnitTesting;
using Xunit;
using AoCday1puzzle1;

namespace AoCTests
{
    public class UnitTest1
    {
        [Fact]
        public void ReturnTwo_Void_ShouldReturnTwo()
        {
            //Arrange
            Depth depth = new Depth();
            int expected = 2;
            //Act
            int actual = depth.returnTwo();
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}
