﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace AoCday1puzzle1
{
    public class Depth
    {
        public List<int> DepthMeasurements { get; set; }
        public List<int> ReducedDepths { get; set; }
        public Depth(List<int> depthMeasurements)
        {
            DepthMeasurements = depthMeasurements;
        }
        public Depth(string link)
        {
            DepthMeasurements = ReadFromExternalFile(link);
        }

        private List<int> ReadFromExternalFile(string link)
        {
            List<int> depthList = new List<int>();
            //string list = "";
            using (var sr = new StreamReader(link))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    int number;
                    if (int.TryParse(line, out number))
                        depthList.Add(number);
                }
            }
            return depthList;

        }

        public int CountIncreases(List<int> depths)
        {
            int count = 0;
            for (int i = 1; i < depths.Count; i++)
            {
                if(depths[i] > depths[i - 1])
                {
                    count++;
                }
            }
            return count;
        }
        public List<int> ReduceDepthMeasurements(List<int> depths)
        {
            List<int> reducedList = new List<int>();
            for (int i = 0; i < depths.Count - 2; i++)
            {
                reducedList.Add(depths[i] + depths[i + 1] + depths[i + 2]);
            }
            return reducedList;
        }
    }
}
