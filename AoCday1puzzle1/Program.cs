﻿using System;

namespace AoCday1puzzle1
{
    class Program
    {
        static void Main(string[] args)
        {
            Depth depth = new Depth("C:/Users/Jasper Klingen/Desktop/input.txt");
            depth.ReducedDepths = depth.ReduceDepthMeasurements(depth.DepthMeasurements);
            Console.WriteLine(depth.CountIncreases(depth.ReducedDepths));
        }
    }
}
