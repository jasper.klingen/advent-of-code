﻿using System;

namespace Day2dive
{
    class Program
    {
        static void Main(string[] args)
        {
            Submarine submarine = new Submarine("C:/Users/Jasper Klingen/Desktop/input2.txt");
            submarine.ReadInstructions(submarine.NavigationInstructions);
            Console.WriteLine(submarine.Distance * submarine.Depth);
        }
    }
}
