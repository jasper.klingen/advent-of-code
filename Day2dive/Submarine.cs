﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Day2dive
{
    public class Submarine
    {
        public int Aim { get; set; }
        public int Depth { get; set; }
        public int Distance { get; set; }
        public List<string> NavigationInstructions { get; set; }
        public Submarine()
        {

        }
        public Submarine(string link)
        {
            NavigationInstructions = ReadFromExternalFile(link);
        }
        private List<string> ReadFromExternalFile(string link)
        {
            List<string> depthList = new List<string>();
            //string list = "";
            using (var sr = new StreamReader(link))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    depthList.Add(line);
                }
            }
            return depthList;

        }
        public void ReadInstructions(List<string> navigationInstructions)
        {
            foreach(string instruction in navigationInstructions)
            {
                string direction = instruction.Split(' ')[0];
                string work = instruction.Split(' ')[1];
                (int dx, int dPitch) = ReadSingleInstruction(direction, work);
                Distance += dx;
                Aim += dPitch;
                Depth += Aim * dx;
            }
        }

        private (int dX, int dPitch) ReadSingleInstruction(string direction, string work)
        {
            switch (direction)
            {
                case "forward":
                    return (int.Parse(work), 0);
                case "down":
                    return (0, int.Parse(work));
                case "up":
                    return (0, -int.Parse(work));
                default:
                    return (0, 0);
            }              
        }
    }
}
