using System;
using Xunit;
using Day2dive;
using System.Collections.Generic;

namespace Day2diveTest
{
    public class UnitTest1
    {
        public List<string> NavigationInstructions { get; set; } = new List<string>
        {
            "forward 5",
            "down 5",
            "forward 8",
            "up 3",
            "down 8",
            "forward 2",
        };
        [Fact]
        public void Test1()
        {
            //Arrange
            Submarine submarine = new Submarine();
            int expected = 900;
            //Act
            submarine.ReadInstructions(NavigationInstructions);
            int actual = submarine.Distance * submarine.Depth;
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
