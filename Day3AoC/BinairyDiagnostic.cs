﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Day3AoC
{
    public class BinairyDiagnostic
    {
        public IEnumerable<bool[]> BitList { get; set; }
        private int NumberOfBits { get; set; } = 5;
        public BinairyDiagnostic(string link)
        {
            List<string> stringbitList = ReadFromExternalFile(link);
            BitList = ConvertStringListToBitList(stringbitList);

        }

        public BinairyDiagnostic(List<string> stringBitList)
        {
            BitList = ConvertStringListToBitList(stringBitList);
        }

        private IEnumerable<bool[]> ConvertStringListToBitList(List<string> bitsList)
        {
            foreach (string s in bitsList)
            {
                NumberOfBits = s.Length;
                bool[] bits = new bool[NumberOfBits];
                for (int i = 0; i < NumberOfBits; i++)
                {
                    if (s[i] == '0')
                        bits[i] = false;
                    else if (s[i] == '1')
                        bits[i] = true;
                    else
                        throw new InvalidDataException();
                }
                yield return bits;
            }
        }

        public int CalcSupportRating()
        {
            return OxygenGeneratorRating(BitList) * CO2ScrubberRating(BitList);
        }

        private int CO2ScrubberRating(IEnumerable<bool[]> bitList)
        {
            throw new NotImplementedException();
        }

        private int OxygenGeneratorRating(IEnumerable<bool[]> bitList)
        {
            var oxygenGeneratorList0 = new List<bool[]>();
            var oxygenGeneratorList1 = new List<bool[]>();

            for (int i = 0; i < NumberOfBits; i++)
            {
                foreach (bool[] b in bitList)
                {
                    if (b[i])
                        oxygenGeneratorList1.Add(b);
                    else
                        oxygenGeneratorList0.Add(b);
                }
                if (oxygenGeneratorList0.Count() > oxygenGeneratorList1.Count())
                    bitList = oxygenGeneratorList0;
                else
                    bitList = oxygenGeneratorList1;

            }
            int bitValue = 0;
            for (int i = 0; i < NumberOfBits; i++)
            {
                if (bitList.First()[i])
                    bitValue += (int)Math.Pow(2, NumberOfBits - i - 1);
            }
            return bitValue;
        }

        private List<string> ReadFromExternalFile(string link)
        {
            var bitsList = new List<string>();
            //string list = "";
            using (var sr = new StreamReader(link))
            {
                string line;
                while ((line = sr.ReadLine()) != null)
                {
                    bitsList.Add(line);
                }
            }
            return bitsList;
        }
        public int CalcPowerConsumption()
        {
            return GammaRate() * EpsilonRate();
        }

        private int EpsilonRate()
        {
            return ((int)Math.Pow(2, NumberOfBits) - 1) - GammaRate();
        }

        private int GammaRate()
        {
            int gammaRate = 0;
            for (int i = 0; i < NumberOfBits; i++)
            {
                int bitsCummulative = 0;
                foreach (bool[] b in BitList)
                {
                    if (b[i])
                        bitsCummulative++;
                    else
                        bitsCummulative--;
                }
                if (bitsCummulative > 0)
                    gammaRate += (int)Math.Pow(2, (NumberOfBits - 1) - i);
            }
            return gammaRate;
        }
    }
}
