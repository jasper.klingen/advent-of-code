using System;
using Xunit;
using Day3AoC;
using System.Collections.Generic;

namespace day3test
{
    public class UnitTest1
    {
        public List<string> MockStringBitList { get; set; } = new List<string>()
        {
"00100",
"11110",
"10110",
"10111",
"10101",
"01111",
"00111",
"11100",
"10000",
"11001",
"00010",
"01010"
        };
        [Fact]
        public void CalcPowerConsumption_MockData_198()
        {
            //Arrange
            var bd = new BinairyDiagnostic(MockStringBitList);
            int expected = 198;
            //Act
            int actual = bd.CalcPowerConsumption();
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void BinairyDiagnostics_MockData_230()
        {
            //Arrange
            var bd = new BinairyDiagnostic(MockStringBitList);
            int expected = 198;
            //Act
            int actual = bd.CalcSupportRating();
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
